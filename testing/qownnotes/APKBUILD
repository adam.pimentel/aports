# Contributor: Francesco Colista <fcolista@alpinelinux.org>
# Maintainer: Francesco Colista <fcolista@alpinelinux.org>
pkgname=qownnotes
pkgver=21.8.1
pkgrel=0
pkgdesc="Plain-text file markdown note taking with Nextcloud/ownCloud integration"
url="https://www.qownnotes.org/"
arch="all !ppc64le !s390x !armhf" # armhf blocked by qt5-qtdeclarative
license="GPL-2.0-only"
makedepends="qt5-qtdeclarative-dev qt5-qtsvg-dev qt5-qtxmlpatterns-dev
	qt5-qttools-dev qt5-qtbase-dev qt5-qtwebsockets-dev botan-dev
	qt5-qtx11extras-dev"
subpackages="$pkgname-lang"
source="https://download.tuxfamily.org/qownnotes/src/qownnotes-$pkgver.tar.xz"

prepare() {
	default_prepare
	echo "#define RELEASE \"Alpine Linux\"" > release.h
}

build() {
	/usr/lib/qt5/bin/qmake \
		QMAKE_CFLAGS_RELEASE="$CFLAGS" \
		QMAKE_CXXFLAGS_RELEASE="$CXXFLAGS" \
		QMAKE_LFLAGS_RELEASE="$LDFLAGS"
	make
}

check() {
	make check
}

package() {
	install -D -m755 QOwnNotes "$pkgdir/usr/bin/QOwnNotes"
	install -D -m644 PBE.QOwnNotes.desktop "$pkgdir/usr/share/applications/PBE.QOwnNotes.desktop"
	install -D -m644 "images/icons/128x128/apps/QOwnNotes.png" "$pkgdir/usr/share/pixmaps/QOwnNotes.png"
	for format in 16x16 24x24 32x32 48x48 64x64 96x96 128x128 256x256 512x512 ; do
		install -D -m644 "images/icons/$format/apps/QOwnNotes.png" "$pkgdir/usr/share/icons/hicolor/$format/apps/QOwnNotes.png"
	done
}

lang() {
	install -d "$subpkgdir/usr/share/QOwnNotes/languages/"
	install -D -m644 "$builddir"/languages/*.qm "$subpkgdir/usr/share/QOwnNotes/languages/"
}

sha512sums="
bcfc346d85fb0b8b3a6c4f43dfbfaad8e2a3494ac35cb4e1912f1d650ad5ac327f11c0b69c47bcff568fd613cb4fbea36e7ad8681c0ea09e615a249a8ac18af0  qownnotes-21.8.1.tar.xz
"
