# Contributor: Michał Polański <michal@polanski.me>
# Maintainer: Michał Polański <michal@polanski.me>
pkgname=tlstunnel
pkgver=0.1.1
pkgrel=0
pkgdesc="TLS reverse proxy"
url="https://sr.ht/~emersion/tlstunnel/"
license="MIT"
arch="all"
options="chmod-clean"
depends="ca-certificates"
makedepends="go scdoc libcap"
subpackages="$pkgname-doc $pkgname-openrc"
pkgusers="$pkgname"
pkggroups="$pkgname"
install="$pkgname.pre-install"
source="$pkgname-v$pkgver.tar.gz::https://git.sr.ht/~emersion/tlstunnel/archive/v$pkgver.tar.gz
	$pkgname.initd
	config
	"
builddir="$srcdir/tlstunnel-v$pkgver"

export GOPATH="$srcdir"
export GOFLAGS="$GOFLAGS -trimpath"

build() {
	make
}

check() {
	go test ./...
}

package() {
	make DESTDIR="$pkgdir" PREFIX=/usr BINDIR=/usr/sbin install
	setcap cap_net_bind_service=+ep "$pkgdir"/usr/sbin/tlstunnel

	install -Dm644 "$srcdir"/config "$pkgdir"/etc/tlstunnel/config

	install -Dm755 "$srcdir"/$pkgname.initd "$pkgdir"/etc/init.d/$pkgname
}

sha512sums="
2656c4f668e99ceca2bfcfac3dbbd14fd35bdafa90fe5ec9cdd26101324bf273b8ba6cfc0dc6d402c0b6ab3d38e92daf0d2c33944f70e4152bd722d14615c711  tlstunnel-v0.1.1.tar.gz
fd781358eb5fd1c1605b9330e0094108d758d9e9664e99a451ba5a58a52f68a4858cd2b6b75fcb3aeec05a81b72758ca49c1f18e6e3ecebe01ccf655ad53d3cc  tlstunnel.initd
a4ec6c1ff057b9ad5d9e9294725dbc03f937669da30956c33c11da86f8122740eb9d4989e7dd0ad3032e7351e8bf1f8fa4d2320771aa24d227ff766d20c05258  config
"
